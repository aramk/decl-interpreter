-------------------------------------------------------------------------------
--
-- vim: sw=4 ts=4 et ft=haskell
--
-- A representation for the ABCD language.
--
--
-- Written by Zoltan Somogyi.
-- Version 1.
--

module Prog(
        MaybeOK(..),
        MaybeOK2(..),
        MaybeOK3(..),
        MaybeDebug(..),
        MaybePrintProg(..),
        Val(..),
        Vars(..),
        Var(..),
        Expr(..),
        Stmt(..),
        Func(..),
        Prog(..)
    ) where

-------------------------------------------------------------------------------
--
-- Utility types.
--

data MaybeOK a = OK a | Error String
    deriving (Show, Read)
data MaybeOK2 a b = OK2 a b | Error2 String
    deriving (Show, Read)
data MaybeOK3 a b c = OK3 a b c | Error3 String
    deriving (Show, Read)

-------------------------------------------------------------------------------
--
-- Types needed by the interpreter.
--

data MaybeDebug = Debug | NoDebug
	deriving Eq
data MaybePrintProg = PrintProg | NoPrintProg

data Val = Num Int | Nil | Cons Val Val
    deriving (Eq, Show, Read)

data Vars = Vars Val Val Val Val
    deriving (Show, Read)

-------------------------------------------------------------------------------
--
-- The representation of ABCD programs.
--

-- the four possible variables
data Var = A | B | C | D
    deriving (Show, Read)

data Expr
    = Var Var
    | NumE Int
    | NilE
    | ConsE Expr Expr

    | Plus Expr Expr
    | Minus Expr Expr
    | Times Expr Expr
    | Div Expr Expr

    | Equal Expr Expr
    | Less Expr Expr
    | Greater Expr Expr
    | Not Expr
    | Isnum Expr
    | And Expr Expr
    | Or Expr Expr

    | Head Expr
    | Tail Expr

    | Call String
    deriving (Show, Read)

data Stmt
    = Assign Var Expr
    | ITE Expr [Stmt] [Stmt]
    | While Expr [Stmt]
    deriving (Show, Read)

data Func = Func {
    func_name   :: String,
    func_body   :: [Stmt]
} deriving (Show, Read)

data Prog = Prog [Func]
    deriving (Show, Read)

data SomeType
    = Unary { u :: Int }
    | Associative { s1 :: SomeType }
    | Binary { s1, s2 :: SomeType }

someFunc :: SomeType -> Int
someFunc s = case s of
    Unary{}       -> u s
    Associative{} -> sf1
    Binary{}      -> sf1 + sf2
  where
    sf1 = someFunc (s1 s)
    sf2 = someFunc (s2 s)
-------------------------------------------------------------------------------
--
-- vim: sw=4 ts=4 et ft=haskell
--
-- A parser for the ABCD language.
--
-- Written by Zoltan Somogyi, Lee Naish.
-- Version 3.
--

module Parse(parse_lumber, parse_prog_file) where

import Data.Char

import Prog

-------------------------------------------------------------------------------
--
-- The parser for Vals (aka Lumbers).
-- We parse an expression and check it just has numbers, cons and []
--

-- intended invocation: parse_lumber line_number contents_of_line_without_nl
parse_lumber :: Int -> String -> MaybeOK Val
parse_lumber line chars =
    let
	tokens = scan (annotate_chars line 1 chars)
        maybe_expr = parse_expr tokens
    in
        case maybe_expr of
            Error2 msg ->
                Error msg
            OK2 expr rest ->
                case rest of
                    [] ->
                        expr_to_val expr
                    (_:_) ->
                        Error ("unexpected characters after expression")

expr_to_val :: Expr -> MaybeOK Val
expr_to_val NilE = OK Nil
expr_to_val (NumE n) = OK (Num n)
expr_to_val (ConsE e1 e2) =
    let
        mv1 = expr_to_val e1
        mv2 = expr_to_val e2
    in
        case mv1 of
            OK v1 ->
                case mv2 of
                    OK v2 ->
                        OK (Cons v1 v2)
                    _ ->
                        Error "non-constant expression"
            _ ->
                Error "non-constant expression"
expr_to_val _ = Error "non-constant expression"


-------------------------------------------------------------------------------
--
-- The parser for ABCD programs.
--

parse_prog_file :: String -> IO (MaybeOK Prog)
parse_prog_file filename = do
    fs <- readFile filename
    let tokens = (scan (annotate_chars 1 1 fs))
    -- putStrLn (show tokens)
    return (parse_prog [] tokens)

-- prog: func+
parse_prog :: [Func] -> [LocToken] -> MaybeOK Prog
parse_prog funcssf0 ts =
    let maybefunc = parse_func ts in
    case maybefunc of
        Error2 msg ->
            Error msg
        OK2 func ts1 ->
            let funcssf1 = funcssf0 ++ [func] in
            case ts1 of
                _:_ ->
                    parse_prog funcssf1 ts1
                [] ->
                    OK (Prog funcssf1)

-- func: FUNC ID BEGIN stmt* END
parse_func :: [LocToken] -> MaybeOK2 Func [LocToken]
parse_func [] =
    Error2 (expgoteof "func")
parse_func ((LocToken line column t0):ts) =
    case t0 of
        TokenFunc ->
            case ts of
                (LocToken _ _ (TokenId funcname)):ts1 ->
                    case ts1 of
                        (LocToken _ _ TokenBegin):ts2 ->
                            let maybebody = parse_func_body [] ts2 in
                            case maybebody of
                                Error2 msg ->
                                    Error2 msg
                                OK2 body ts3 ->
                                    OK2 (Func funcname body) ts3
                        (LocToken line2 column2 t2):_ ->
                            Error2 (expgot line2 column2 "begin" (show t2))
                        [] ->
                            Error2 (expgoteof "begin")
                (LocToken line1 column1 t1):_ ->
                    Error2 (expgot line1 column1 "function name" (show t1))
                [] ->
                    Error2 (expgoteof "function name")
        _ ->
            Error2 (expgot line column "func" (show t0))

parse_func_body :: [Stmt] -> [LocToken] -> MaybeOK2 [Stmt] [LocToken]
parse_func_body _ [] =
    Error2 (expgoteof "statement")
parse_func_body stmtssf ts0@((LocToken _ _ t0):ts1) =
    case t0 of
        TokenEnd ->
            OK2 stmtssf ts1
        _ ->
            let maybestmt = parse_stmt ts0 in
            case maybestmt of
                Error2 msg ->
                    Error2 msg
                OK2 stmt ts2 ->
                    parse_func_body (stmtssf ++ [stmt]) ts2

-- stmt: VAR ASSIGN expr SEMI
-- stmt: IF expr THEN stmt* (ELSE stmt*)? ENDIF
-- stmt: WHILE expr DO stmt* DONE
parse_stmt :: [LocToken] -> MaybeOK2 Stmt [LocToken]
parse_stmt [] =
    Error2 (expgoteof "statement")
parse_stmt ((LocToken line column t0):ts) =
    case t0 of
        TokenVar varname ->
            case ts of
                (LocToken _ _ TokenAssign):ts2 ->
                    let maybeexpr = parse_expr ts2 in
                    case maybeexpr of
                        Error2 msg ->
                            Error2 msg
                        OK2 expr ts3 ->
                            case ts3 of
                                (LocToken _ _ TokenSemi):ts4 ->
                                    OK2 (Assign varname expr) ts4
                                (LocToken line1 column1 t3):_ ->
                                    Error2 (expgot line1 column1
                                        "semicolon" (show t3))
                                [] ->
                                    Error2 (expgoteof "semicolon")
                (LocToken line1 column1 t1):_ ->
                    Error2 (expgot line1 column1
                        "assignment operator" (show t1))
                [] ->
                    Error2 (expgoteof "assignment operator")
        TokenWhile ->
            let maybeexpr = parse_expr ts in
            case maybeexpr of
                Error2 msg ->
                    Error2 msg
                OK2 expr ts1 ->
                    case ts1 of
                        (LocToken _ _ TokenDo):ts2 ->
                            let maybebody = parse_while_body [] ts2 in
                            case maybebody of
                                Error2 msg ->
                                    Error2 msg
                                OK2 body ts3 ->
                                    OK2 (While expr body) ts3
                        (LocToken line1 column1 t1):_ ->
                            Error2 (expgot line1 column1 "do" (show t1))
                        [] ->
                            Error2 (expgoteof "do")
        TokenIf ->
            let maybeexpr = parse_expr ts in
            case maybeexpr of
                Error2 msg ->
                    Error2 msg
                OK2 expr ts1 ->
                    case ts1 of
                        (LocToken _ _ TokenThen):ts2 ->
                            let maybethenelse = parse_then [] ts2 in
                            case maybethenelse of
                                Error3 msg ->
                                    Error2 msg
                                OK3 thenstmt elsestmt ts3 ->
                                    OK2 (ITE expr thenstmt elsestmt) ts3
                        (LocToken line1 column1 t1):_ ->
                            Error2 (expgot line1 column1 "then" (show t1))
                        [] ->
                            Error2 (expgoteof "then")
        _ ->
            Error2 (expgot line column "variable, if or while" (show t0))

-- then: stmt* (ELSE stmt*)? ENDIF
parse_then :: [Stmt] -> [LocToken] -> MaybeOK3 [Stmt] [Stmt] [LocToken]
parse_then thensf [] = OK3 thensf [] []
parse_then thensf ts0@((LocToken _ _ t0):ts1) =
    case t0 of
        TokenEndif ->
            OK3 thensf [] ts1
        TokenElse ->
            let maybeelse = parse_else [] ts1 in
            case maybeelse of
                Error2 msg ->
                    Error3 msg
                OK2 elsestmt ts2 ->
                    OK3 thensf elsestmt ts2
        _ ->
            let maybestmt = parse_stmt ts0 in
            case maybestmt of
                Error2 msg ->
                    Error3 msg
                OK2 stmt ts2 ->
                    parse_then (thensf ++ [stmt]) ts2

-- else: stmt* ENDIF
parse_else :: [Stmt] -> [LocToken] -> MaybeOK2 [Stmt] [LocToken]
parse_else elsesf [] = OK2 elsesf []
parse_else elsesf ts0@((LocToken _ _ t0):ts1) =
    case t0 of
        TokenEndif ->
            OK2 elsesf ts1
        _ ->
            let maybestmt = parse_stmt ts0 in
            case maybestmt of
                Error2 msg ->
                    Error2 msg
                OK2 stmt ts2 ->
                    parse_else (elsesf ++ [stmt]) ts2

-- while_body: stmt* DONE
parse_while_body :: [Stmt] -> [LocToken] -> MaybeOK2 [Stmt] [LocToken]
parse_while_body bodysf [] = OK2 bodysf []
parse_while_body bodysf ts0@((LocToken _ _ t0):ts1) =
    case t0 of
        TokenDone ->
            OK2 bodysf ts1
        _ ->
            let maybestmt = parse_stmt ts0 in
            case maybestmt of
                Error2 msg ->
                    Error2 msg
                OK2 stmt ts2 ->
                    parse_while_body (bodysf ++ [stmt]) ts2

-- expr: cons
parse_expr :: [LocToken] -> MaybeOK2 Expr [LocToken]
parse_expr ts = parse_cons ts

-- cons: disj (CONS disj)*
parse_cons :: [LocToken] -> MaybeOK2 Expr [LocToken]
parse_cons ts0 =
    let maybeelt1 = parse_disj ts0 in
    case maybeelt1 of
        Error2 msg ->
            Error2 msg
        OK2 elt1 ts1 ->
            parse_cons_aux elt1 ts1

parse_cons_aux :: Expr -> [LocToken] -> MaybeOK2 Expr [LocToken]
parse_cons_aux elt1 [] = OK2 elt1 []
parse_cons_aux elt1 ts0@((LocToken _ _ t0):ts1) =
    case t0 of
        TokenCons ->
            let maybeelt2 = parse_disj ts1 in
            case maybeelt2 of
                Error2 msg ->
                    Error2 msg
                OK2 elt2 ts2 ->
                    parse_cons_aux (ConsE elt1 elt2) ts2
        _ ->
            OK2 elt1 ts0

-- disj: conj (OR conj)*
parse_disj :: [LocToken] -> MaybeOK2 Expr [LocToken]
parse_disj ts0 =
    let maybedisjunct1 = parse_conj ts0 in
    case maybedisjunct1 of
        Error2 msg ->
            Error2 msg
        OK2 disjunct1 ts1 ->
            parse_disj_aux disjunct1 ts1

parse_disj_aux :: Expr -> [LocToken] -> MaybeOK2 Expr [LocToken]
parse_disj_aux disjunct1 [] = OK2 disjunct1 []
parse_disj_aux disjunct1 ts0@((LocToken _ _ t0):ts1) =
    case t0 of
        TokenOr ->
            let maybedisjunct2 = parse_conj ts1 in
            case maybedisjunct2 of
                Error2 msg ->
                    Error2 msg
                OK2 disjunct2 ts2 ->
                    parse_disj_aux (Or disjunct1 disjunct2) ts2
        _ ->
            OK2 disjunct1 ts0

-- conj: bool (AND bool)*
parse_conj :: [LocToken] -> MaybeOK2 Expr [LocToken]
parse_conj ts0 =
    let maybeconjunct1 = parse_bool ts0 in
    case maybeconjunct1 of
        Error2 msg ->
            Error2 msg
        OK2 conjunct1 ts1 ->
            parse_conj_aux conjunct1 ts1

parse_conj_aux :: Expr -> [LocToken] -> MaybeOK2 Expr [LocToken]
parse_conj_aux conjunct1 [] = OK2 conjunct1 []
parse_conj_aux conjunct1 ts0@((LocToken _ _ t0):ts1) =
    case t0 of
        TokenAnd ->
            let maybeconjunct2 = parse_bool ts1 in
            case maybeconjunct2 of
                Error2 msg ->
                    Error2 msg
                OK2 conjunct2 ts2 ->
                    parse_conj_aux (And conjunct1 conjunct2) ts2
        _ ->
            OK2 conjunct1 ts0

-- bool: cmp | NOT cmp | ISNUM factor
parse_bool :: [LocToken] -> MaybeOK2 Expr [LocToken]
parse_bool [] =
    Error2 (expgoteof "expression")
parse_bool ts0@((LocToken _ _ t0):ts1) =
    case t0 of
        TokenNot ->
            let maybecmp1 = parse_cmp ts1 in
            case maybecmp1 of
                Error2 msg ->
                    Error2 msg
                OK2 cmp1 ts2 ->
                    OK2 (Not cmp1) ts2
        _ ->
            parse_cmp ts0

-- cmp: sum | sum (EQ|LT|GT) sum
parse_cmp :: [LocToken] -> MaybeOK2 Expr [LocToken]
parse_cmp ts0 =
    let maybesum1 = parse_sum ts0 in
    case maybesum1 of
        Error2 msg ->
            Error2 msg
        OK2 sum1 ts1 ->
            case ts1 of
                (LocToken _ _ TokenEq):ts2 ->
                    let maybesum2 = parse_sum ts2 in
                    case maybesum2 of
                        Error2 msg ->
                            Error2 msg
                        OK2 sum2 ts3 ->
                            OK2 (Equal sum1 sum2) ts3
                (LocToken _ _ TokenLt):ts2 ->
                    let maybesum2 = parse_sum ts2 in
                    case maybesum2 of
                        Error2 msg ->
                            Error2 msg
                        OK2 sum2 ts3 ->
                            OK2 (Less sum1 sum2) ts3
                (LocToken _ _ TokenGt):ts2 ->
                    let maybesum2 = parse_sum ts2 in
                    case maybesum2 of
                        Error2 msg ->
                            Error2 msg
                        OK2 sum2 ts3 ->
                            OK2 (Greater sum1 sum2) ts3
                _ ->
                    OK2 sum1 ts1

-- sum: prod ((PLUS|MINUS) prod)*
parse_sum :: [LocToken] -> MaybeOK2 Expr [LocToken]
parse_sum ts0 =
    let maybeterm1 = parse_prod ts0 in
    case maybeterm1 of
        Error2 msg ->
            Error2 msg
        OK2 term1 ts1 ->
            parse_sum_aux term1 ts1

parse_sum_aux :: Expr -> [LocToken] -> MaybeOK2 Expr [LocToken]
parse_sum_aux term1 [] = OK2 term1 []
parse_sum_aux term1 ts0@((LocToken _ _ t0):ts1) =
    case t0 of
        TokenPlus ->
            let maybeterm2 = parse_prod ts1 in
            case maybeterm2 of
                Error2 msg ->
                    Error2 msg
                OK2 term2 ts2 ->
                    parse_sum_aux (Plus term1 term2) ts2
        TokenMinus ->
            let maybeterm2 = parse_prod ts1 in
            case maybeterm2 of
                Error2 msg ->
                    Error2 msg
                OK2 term2 ts2 ->
                    parse_sum_aux (Minus term1 term2) ts2
        _ ->
            OK2 term1 ts0

-- prod: factor ((TIMES|DIV) factor)*
parse_prod :: [LocToken] -> MaybeOK2 Expr [LocToken]
parse_prod ts0 =
    let maybefactor1 = parse_factor ts0 in
    case maybefactor1 of
        Error2 msg ->
            Error2 msg
        OK2 factor1 ts1 ->
            parse_prod_aux factor1 ts1

parse_prod_aux :: Expr -> [LocToken] -> MaybeOK2 Expr [LocToken]
parse_prod_aux factor1 [] = OK2 factor1 []
parse_prod_aux factor1 ts0@((LocToken _ _ t0):ts1) =
    case t0 of
        TokenTimes ->
            let maybefactor2 = parse_factor ts1 in
            case maybefactor2 of
                Error2 msg ->
                    Error2 msg
                OK2 factor2 ts2 ->
                    parse_prod_aux (Times factor1 factor2) ts2
        TokenDiv ->
            let maybefactor2 = parse_factor ts1 in
            case maybefactor2 of
                Error2 msg ->
                    Error2 msg
                OK2 factor2 ts2 ->
                    parse_prod_aux (Div factor1 factor2) ts2
        _ ->
            OK2 factor1 ts0

-- factor: VAR | NUM | NIL | LP expr RP | CALL LP ID RP
parse_factor :: [LocToken] -> MaybeOK2 Expr [LocToken]
parse_factor [] =
    Error2 (expgoteof "expression")
parse_factor ((LocToken line0 column0 t0):ts1) =
    case t0 of
        TokenVar varname ->
	    OK2 (Var varname) ts1
        TokenNum num ->
            OK2 (NumE num) ts1
        TokenNil ->
            OK2 NilE ts1
        TokenLp ->
            let maybesubexpr = parse_expr ts1 in
            case maybesubexpr of
                Error2 msg ->
                    Error2 msg
                OK2 subexpr ts2 ->
                    case ts2 of
                        (LocToken _ _ TokenRp):ts3 ->
                            OK2 subexpr ts3
                        (LocToken line2 column2 t2):_ ->
                            Error2 (expgot line2 column2
                                "right parenthesis" (show t2))
                        [] ->
                            Error2 (expgoteof "right parenthesis")
        TokenCall ->
            case ts1 of
                (LocToken _ _ TokenLp):ts2 ->
                    case ts2 of
                        (LocToken _ _ (TokenId callee)):ts3 ->
                            case ts3 of
                                (LocToken _ _ TokenRp):ts4 ->
                                    OK2 (Call callee) ts4
                                (LocToken line3 column3 t3):_ ->
                                    Error2 (expgot line3 column3
                                        "right parenthesis" (show t3))
                                [] ->
                                    Error2 (expgoteof "right parenthesis")
                        (LocToken line2 column2 t2):_ ->
                            Error2 (expgot line2 column2
                                "function name" (show t2))
                        [] ->
                            Error2 (expgoteof "function name")
                (LocToken line1 column1 t1):_ ->
                    Error2 (expgot line1 column1 "left parenthesis" (show t1))
                [] ->
                    Error2 (expgoteof "left parenthesis")
        TokenHead ->
            case ts1 of
                (LocToken _ _ TokenLp):ts2 ->
                    let maybeexpr = parse_expr ts2 in
                    case maybeexpr of
                        Error2 msg ->
                            Error2 msg
                        OK2 expr ts3 ->
                            case ts3 of
                                (LocToken _ _ TokenRp):ts4 ->
                                    OK2 (Head expr) ts4
                                (LocToken line3 column3 t3):_ ->
                                    Error2 (expgot line3 column3
                                        "right parenthesis" (show t3))
                                [] ->
                                    Error2 (expgoteof "right parenthesis")
                (LocToken line1 column1 t1):_ ->
                    Error2 (expgot line1 column1 "left parenthesis" (show t1))
                [] ->
                    Error2 (expgoteof "left parenthesis")
        TokenTail ->
            case ts1 of
                (LocToken _ _ TokenLp):ts2 ->
                    let maybeexpr = parse_expr ts2 in
                    case maybeexpr of
                        Error2 msg ->
                            Error2 msg
                        OK2 expr ts3 ->
                            case ts3 of
                                (LocToken _ _ TokenRp):ts4 ->
                                    OK2 (Tail expr) ts4
                                (LocToken line3 column3 t3):_ ->
                                    Error2 (expgot line3 column3
                                        "right parenthesis" (show t3))
                                [] ->
                                    Error2 (expgoteof "right parenthesis")
                (LocToken line1 column1 t1):_ ->
                    Error2 (expgot line1 column1 "left parenthesis" (show t1))
                [] ->
                    Error2 (expgoteof "left parenthesis")
        TokenIsnum ->
            case ts1 of
                (LocToken _ _ TokenLp):ts2 ->
                    let maybeexpr = parse_expr ts2 in
                    case maybeexpr of
                        Error2 msg ->
                            Error2 msg
                        OK2 expr ts3 ->
                            case ts3 of
                                (LocToken _ _ TokenRp):ts4 ->
                                    OK2 (Isnum expr) ts4
                                (LocToken line3 column3 t3):_ ->
                                    Error2 (expgot line3 column3
                                        "right parenthesis" (show t3))
                                [] ->
                                    Error2 (expgoteof "right parenthesis")
                (LocToken line1 column1 t1):_ ->
                    Error2 (expgot line1 column1 "left parenthesis" (show t1))
                [] ->
                    Error2 (expgoteof "left parenthesis")
        _ ->
            Error2 (expgot line0 column0 "expression" (show t0))

-------------------------------------------------------------------------------
--
-- Functions for error handling in the parser.
--

expgot :: Int -> Int -> String -> String -> String
expgot line column expected got =
    "line " ++ show line ++ ", " ++
    "column " ++ show column ++ ": " ++
    "expected " ++ expected ++ ", got " ++ got

expgoteof :: String -> String
expgoteof expected =
    "at end-of-file: expected " ++ expected

-------------------------------------------------------------------------------
--
-- The scanner for ABCD programs.
--

data Token
    = TokenNum Int
    | TokenNil
    | TokenCons
    | TokenPlus
    | TokenMinus
    | TokenTimes
    | TokenDiv
    | TokenEq
    | TokenLt
    | TokenGt

    | TokenLp
    | TokenRp

    | TokenNot
    | TokenIsnum
    | TokenAnd
    | TokenOr

    | TokenAssign
    | TokenSemi
    | TokenIf
    | TokenThen
    | TokenElse
    | TokenEndif
    | TokenWhile
    | TokenDo
    | TokenDone

    | TokenFunc
    | TokenBegin
    | TokenEnd

    | TokenHead
    | TokenTail
    | TokenCall

    | TokenVar Var
    | TokenId String

    | TokenBad Char -- the unexpected character
    deriving (Read, Show)

data LocToken = LocToken {
    line_number ::      Int,
    column_number ::    Int,
    token ::            Token
} deriving (Read, Show)

-------------------------------------------------------------------------------

scan :: [AnnoChar] -> [LocToken]
scan [] = []
scan ((AnnoChar line column c):annocs1) =
    if isSpace c then
        scan annocs1
    else if c == '+' then
        (LocToken line column TokenPlus):(scan annocs1)
    else if c == '-' then
        case annocs1 of
            (AnnoChar _ _ '-'):annocs2 ->
                scan (skip_comment annocs2)
            _:_ ->
                (LocToken line column TokenMinus):(scan annocs1)
            [] ->
                (LocToken line column TokenMinus):(scan annocs1)
    else if c == '*' then
        (LocToken line column TokenTimes):(scan annocs1)
    else if c == '/' then
        (LocToken line column TokenDiv):(scan annocs1)
    else if c == '=' then
        (LocToken line column TokenEq):(scan annocs1)
    else if c == '<' then
        (LocToken line column TokenLt):(scan annocs1)
    else if c == '>' then
        (LocToken line column TokenGt):(scan annocs1)
    else if c == '(' then
        (LocToken line column TokenLp):(scan annocs1)
    else if c == ')' then
        (LocToken line column TokenRp):(scan annocs1)
    else if c == ';' then
        (LocToken line column TokenSemi):(scan annocs1)
    else if c == '^' then
        (LocToken line column TokenCons):(scan annocs1)
    else if c == ':' then
        case annocs1 of
            (AnnoChar _ _ '='):annocs2 ->
                (LocToken line column TokenAssign):(scan annocs2)
            _:_ ->
                (LocToken line column (TokenBad c)):(scan annocs1)
            [] ->
                (LocToken line column (TokenBad c)):(scan annocs1)
    else if c == '[' then
        case annocs1 of
            (AnnoChar _ _ ']'):annocs2 ->
                (LocToken line column TokenNil):(scan annocs2)
            _:_ ->
                (LocToken line column (TokenBad c)):(scan annocs1)
            [] ->
                (LocToken line column (TokenBad c)):(scan annocs1)
    else if isDigit c then
        let (num, annocs2) = scan_num (digitToInt c) annocs1 in
        (LocToken line column (TokenNum num)):(scan annocs2)
    else if c == '_' || isAlpha c then
        let (lexeme, annocs2) = scan_id [c] annocs1 in
        case lexeme of
            "not" ->
                (LocToken line column TokenNot):(scan annocs2)
            "isnum" ->
                (LocToken line column TokenIsnum):(scan annocs2)
            "and" ->
                (LocToken line column TokenAnd):(scan annocs2)
            "or" ->
                (LocToken line column TokenOr):(scan annocs2)
            "if" ->
                (LocToken line column TokenIf):(scan annocs2)
            "then" ->
                (LocToken line column TokenThen):(scan annocs2)
            "else" ->
                (LocToken line column TokenElse):(scan annocs2)
            "endif" ->
                (LocToken line column TokenEndif):(scan annocs2)
            "while" ->
                (LocToken line column TokenWhile):(scan annocs2)
            "do" ->
                (LocToken line column TokenDo):(scan annocs2)
            "done" ->
                (LocToken line column TokenDone):(scan annocs2)
            "call" ->
                (LocToken line column TokenCall):(scan annocs2)
            "head" ->
                (LocToken line column TokenHead):(scan annocs2)
            "tail" ->
                (LocToken line column TokenTail):(scan annocs2)
            "func" ->
                (LocToken line column TokenFunc):(scan annocs2)
            "begin" ->
                (LocToken line column TokenBegin):(scan annocs2)
            "end" ->
                (LocToken line column TokenEnd):(scan annocs2)
            "a" ->
                (LocToken line column (TokenVar A)):(scan annocs1)
            "b" ->
                (LocToken line column (TokenVar B)):(scan annocs1)
            "c" ->
                (LocToken line column (TokenVar C)):(scan annocs1)
            "d" ->
                (LocToken line column (TokenVar D)):(scan annocs1)
            _ ->
                (LocToken line column (TokenId lexeme)):(scan annocs2)
    else
        (LocToken line column (TokenBad c)):(scan annocs1)

skip_comment :: [AnnoChar] -> [AnnoChar]
skip_comment [] = []
skip_comment ((AnnoChar _ _ c):annocs1) =
    if c == '\n'
    then annocs1
    else skip_comment annocs1

skip_ws :: [AnnoChar] -> [AnnoChar]
skip_ws [] = []
skip_ws annocs0@((AnnoChar _ _ c):annocs1) =
    if isSpace c
    then skip_ws annocs1
    else annocs0

scan_num :: Int -> [AnnoChar] -> (Int, [AnnoChar])
scan_num nsf [] = (nsf, [])
scan_num nsf annocs0@((AnnoChar _ _ c):annocs1) =
    if isDigit c
    then scan_num (nsf * 10 + digitToInt c) annocs1
    else (nsf, annocs0)

scan_id :: String -> [AnnoChar] -> (String, [AnnoChar])
scan_id idsf [] = (idsf, [])
scan_id idsf annocs0@((AnnoChar _ _ c):annocs1) =
    if c == '_' || isAlphaNum c
    then scan_id (idsf ++ [c]) annocs1
    else (idsf, annocs0)

-------------------------------------------------------------------------------

data AnnoChar = AnnoChar {
    char_line_number ::      Int,
    char_column_number ::    Int,
    char ::                 Char
} deriving (Show, Read)

annotate_chars :: Int -> Int -> [Char] -> [AnnoChar]
annotate_chars _ _ [] = []
annotate_chars line column (c:cs) = (annochar:annochars)
    where
        annochar = AnnoChar line column c
        annochars =
            if c == '\n'
            then annotate_chars (line+1) 1 cs
            else annotate_chars line (column+1) cs

-------------------------------------------------------------------------------

./abcdi ../abcd_progs/p42.abcd < ../abcd_progs/in1
./abcdi ../abcd_progs/ops.abcd < ../abcd_progs/in2
./abcdi -ereverse ../abcd_progs/reverse.abcd < ../abcd_progs/in2
./abcdi -esum_w ../abcd_progs/sum_w.abcd < ../abcd_progs/in2
./abcdi -eapp_r ../abcd_progs/app_r.abcd < ../abcd_progs/in2
./abcdi -erange_list ../abcd_progs/range_list.abcd < ../abcd_progs/in1
./abcdi -egcd ../abcd_progs/gcd.abcd < ../abcd_progs/in1
./abcdi -erange_list_r ../abcd_progs/range_list_r.abcd < ../abcd_progs/in1
./abcdi ../abcd_progs/sum_r.abcd < ../abcd_progs/in1
./abcdi -esum_tr ../abcd_progs/sum_tr.abcd < ../abcd_progs/in3
./abcdi -eappend ../abcd_progs/append.abcd < ../abcd_progs/in2
./abcdi ../abcd_progs/sumg_r.abcd < ../abcd_progs/in1
./abcdi -esum ../abcd_progs/sumg.abcd < ../abcd_progs/in3
./abcdi -esort ../abcd_progs/sort.abcd < ../abcd_progs/in2
./abcdi ../abcd_progs/treesort.abcd < ../abcd_progs/in2
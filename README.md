# ABCD Interpreter
University of Melbourne, COMP30020 Declarative Programming, Project 1  
By [Aram Kocharyan](http://aramk.com), 2012

## About

This was a project teaching Haskell. I wrote the interpreter code in `Interpret.hs`. See `spec.html` for the project specification.

## Usage
abcdi is a compiled sample solution is a compiled sample solution
You can try the following, for example:

	./abcdi ../abcd_progs/treesort.abcd < ../abcd_progs/in3

Hopefully it will print `(1^(2^(3^(4^(5^(6^[]))))))`

	../abcd_progs contains some sample ABCD programs

You should copy the following files (and some abcd programs) to your
directory and get to work:

	Makefile makes ./abcdi
	Main.hs is the top level code
	Parse.hs is the parser for ABCD programs
	Prog.hs is the representation used for ABCD programs
	Interpret.hs is the code you have to complete and submit

## ABCD Syntax

Syntax of abcd programs (* means zero or more times, + means one or more
times, ? means zero or one times).  Haskell-style comments are supported
(from -- until the end of the line).

	prog ::= func+
	func ::= "func" id "begin" stmt* "end"
	stmt ::= var ":=" expr ";" |
		"if" expr "then" stmt* (else stmt*)? "endif" |
		 "while" expr "do" stmt* "done"
	expr ::= cons
	cons ::= disj ("^" disj)*
	disj ::= conj ("or" conj)*
	conj ::= bool ("and" bool)*
	bool ::= cmp | "not" cmp
	cmp ::= sum | sum ("=" | "<" | ">") sum
	sum ::= prod (("+" | "-") prod)*
	prod ::= factor (("*" | "/") factor)*
	factor ::= var | num | "[]" | "(" expr ")" | "call" "(" id ")" |
		"head" "(" expr ")" | "tail" "(" expr ")" | "isnum" "(" expr ")"
	var ::= "a" | "b" | "c" | "d"
	num :== sequence of digits
	id ::= letter/underscore followed by letters/underscores/digits
	

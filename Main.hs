-------------------------------------------------------------------------------
--
-- vim: sw=4 ts=4 et ft=haskell
--
-- The top level of an interpreter for the ABCD language.
--
-- Written by Zoltan Somogyi.
-- Version 2.
--

module Main(main) where

import Data.List
import System.Environment

import Prog
import Parse
import Interpret

main :: IO ()
main = do
    progname <- getProgName
    args <- getArgs
    process_args progname args "main" NoDebug NoPrintProg

process_args :: String -> [String] -> String ->
    MaybeDebug -> MaybePrintProg -> IO ()
process_args progname [] _ _ _ = do
    usage progname
process_args progname (arg:args) entry debug print_prog =
    if arg == "-d" then
        process_args progname args entry Debug print_prog
    else if arg == "-p" then
        process_args progname args entry debug PrintProg
    else if isPrefixOf "-e" arg then
        process_args progname args (drop 2 arg) debug print_prog
    else if isPrefixOf "-" arg then
        usage progname
    else if args == [] then
        process_prog progname arg entry debug print_prog
    else
        usage progname

process_prog :: String -> String -> String ->
    MaybeDebug -> MaybePrintProg -> IO ()
process_prog progname filename entry debug print_prog =
    if isSuffixOf ".abcd" filename then
        do
            maybeprog <- parse_prog_file filename
            maybe_read_regs_and_interpret maybeprog entry debug print_prog
    else
        usage progname

usage :: String -> IO ()
usage progname =
    putStrLn ("Usage: " ++ progname ++ " [-d] [-p] [-efunc] filename.abcd")

maybe_read_regs_and_interpret ::
    MaybeOK Prog -> String -> MaybeDebug -> MaybePrintProg -> IO ()
maybe_read_regs_and_interpret (Error msg) _ _ _ = do
    putStrLn ("error: " ++ msg)
maybe_read_regs_and_interpret (OK prog) entry debug NoPrintProg =
    read_regs_and_interpret prog entry debug
maybe_read_regs_and_interpret (OK prog) entry debug PrintProg = do
    putStrLn (show prog)
    read_regs_and_interpret prog entry debug

read_regs_and_interpret :: Prog -> String -> MaybeDebug -> IO ()
read_regs_and_interpret prog entry debug = do
    a <- read_var 1
    b <- read_var 2
    c <- read_var 3
    d <- read_var 4
    let vars = Vars a b c d
    interpret prog vars entry debug

read_var :: Int -> IO Val
read_var line_number = do
    line_chars <- getLine
    let maybe_lumber = parse_lumber line_number line_chars
    case maybe_lumber of
        Error msg ->
            do
                putStrLn ("Error on variable line " ++ (show line_number) ++
                    ": " ++ msg ++ ".")
                putStrLn "Substituting zero."
                return (Num 0)
        OK lumber ->
            return lumber

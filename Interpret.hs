-------------------------------------------------------------------------------
--
-- vim: sw=4 ts=4 et ft=haskell
--
-- An interpreter for the ABCD language.
--
-- HIDE_START
-- Written by Aram Kocharyan, aramk
-- HIDE_END
--

module Interpret(interpret) where

import Prog

import System.IO.Unsafe
import Control.Monad
import Char

-- In a call to this function such as "interpret prog vars entry debug":
-- prog is the ABCD program to be interpreted;
-- vars represents the initial values of the four variables;
-- entry is the name of the entry point function, "main" by default; and
-- debug specifies whether the user wants debugging output.

interpret :: Prog -> Vars -> String -> MaybeDebug -> IO ()
interpret prog vars entry debug = do
	let context = Context prog vars entry debug 0
	let newContext = runFunc entry context
	let output =
		case newContext of
			IError s -> "abcdi: " ++ s
			IOK c -> (strVal (getVar A (cVars c))) ++ "\n"
	putStrLn output

-- DATA ========================================================================

-- I wanted to define MaybeOK as a monad, since this will prevent long chains of
-- error checking each return value for the Error case.
-- However, I couldn't use MaybeOK because of this GHC warning:
--     Warning: orphan instance: instance Monad MaybeOK
-- This is because MaybeOK is defined in Prog.hs, not here.
-- Following advice here:
--     http://stackoverflow.com/questions/3079537/orphaned-instances-in-haskell
--     http://www.haskell.org/haskellwiki/Orphan_instance
-- I decided to define my own MaybeOK variant for use in the interpreter to be
-- safe than sorry. If possible, I'd put a MaybeOK monad declaration in Prog.hs.

data MaybeIOK a = IOK a | IError String deriving (Show, Read)
instance Monad MaybeIOK where
    return x = IOK x  
    IError s >>= _ = IError s
    IOK x >>= f = f x
    fail s = IError s

-- This context is passed to evaluation functions. It stores the current program
-- containing valid functions, current variable values, the function name,
-- debugger status and call stack depth.
data Context = Context {
	cProg :: Prog,
	cVars :: Vars,
	cFunc :: String,
	cDebug:: MaybeDebug,
	cStack :: Int}

-- EVALUATION ==================================================================

-- MaybeIOK is used throughout wherever an IError may be generated.
-- The MaybeIOK monad removes the need to check for IError at each step.

-- Evaluates all Expr into Val
evalExpr :: Expr -> Context -> MaybeIOK Val
evalExpr e c =
	case e of
		-- These map directly to their Var counterparts
		Var v -> IOK (getVar v (cVars c))
		NumE n -> IOK (Num n)
		NilE -> IOK Nil
		ConsE e1 e2 -> do
			v1 <- evalExpr e1 c
			v2 <- evalExpr e2 c
			IOK (Cons v1 v2)
		
		-- Operators
		Plus e1 e2 -> eval2Exprs e1 e2 c evalPlus
		Minus e1 e2 -> eval2Exprs e1 e2 c evalMinus
		Times e1 e2 -> eval2Exprs e1 e2 c evalTimes
		Div e1 e2 -> eval2Exprs e1 e2 c evalDiv
		
		-- Boolean
		Equal e1 e2 -> do
			v1 <- evalExpr e1 c
			v2 <- evalExpr e2 c
			IOK $ evalFromBool (evalEqual v1 v2)
			
		Less e1 e2 -> do
			v1 <- evalExpr e1 c
			v2 <- evalExpr e2 c
			r <- evalLess v1 v2
			IOK $ evalFromBool r
			
		Greater e1 e2 -> do
			v1 <- evalExpr e1 c
			v2 <- evalExpr e2 c
			r <- evalGreater v1 v2
			IOK $ evalFromBool r
			
		Not e1 -> do
			v1 <- evalExpr e1 c
			IOK $ evalFromBool $ not $ evalToBool v1
			
		Isnum e1 -> do
			v1 <- evalExpr e1 c
			IOK $ evalFromBool $ evalIsNum v1
			
		And e1 e2 -> do
			v1 <- evalExpr e1 c
			v2 <- evalExpr e2 c
			IOK $ evalFromBool (evalToBool v1 && evalToBool v2)
			
		Or e1 e2 -> do
			v1 <- evalExpr e1 c
			v2 <- evalExpr e2 c
			IOK $ evalFromBool (evalToBool v1 || evalToBool v2)
	
		-- List/Tree Operators
		Head e1 -> do
			v1 <- evalExpr e1 c
			evalHead v1
			
		Tail e1 -> do
			v1 <- evalExpr e1 c
			evalTail v1
	
		Call funcName -> do
			-- The A Var from the returned Context of the function call is used
			nc <- runFunc funcName c
			IOK $ getVar A $ cVars nc

-- This utility method refactors the case when two Val are used in a function
-- and may generate an IError.
eval2Exprs :: Expr -> Expr -> Context -> (Val -> Val -> MaybeIOK Val)
	-> MaybeIOK Val
eval2Exprs e1 e2 c f = do
	v1 <- evalExpr e1 c
	v2 <- evalExpr e2 c
	f v1 v2

-- Arithmetic ------------------------------------------------------------------

-- Only applied to Num and Nil.

-- Between a Num and Nil, the Num is returned unchanged.
-- Num + Num is simple integer addition.
evalPlus :: Val -> Val -> MaybeIOK Val
evalPlus x Nil = IOK x
evalPlus Nil x = IOK x
-- TODO Cons +
evalPlus (Num i) (Num j) = IOK $ Num (i + j)
evalPlus _ _ = IError "invalid use of (+) operator"

-- Num - Num is simple integer subtraction.
-- Nil is considered 0.
evalMinus :: Val -> Val -> MaybeIOK Val
evalMinus n@(Num _) Nil = IOK n
evalMinus Nil (Num x) = IOK $ Num (-(x))
evalMinus (Num i) (Num j) = IOK $ Num (i - j)
evalMinus _ _ = IError "invalid use of (-) operator"

-- Only Num is considered for multiplication.
-- This avoids getting 0 when multiplying a Val with a Nil (undefined) variable.
evalTimes :: Val -> Val -> MaybeIOK Val
evalTimes (Num i) (Num j) = IOK $ Num (i * j)
evalTimes _ _ = IError "invalid use of (*) operator"

-- Only integer division is considered, and only between Num.
-- Can't divide by 0.
evalDiv :: Val -> Val -> MaybeIOK Val
evalDiv (Num _) (Num 0) = IError "divide by zero, take cover!"
evalDiv (Num i) (Num j) = IOK (Num (i `div` j))
evalDiv _ _ = IError "invalid use of (/) operator"

-- Boolean ---------------------------------------------------------------------

-- True and False represented as Num 1 and Num 0 respectively
evalFromBool :: Bool -> Val
evalFromBool b = Num ( (b) ? (1,0) )

-- A number above 0 is considered True, since Num 0 represents False
-- Nil is considered False
-- Cons are considered True, since they are not Nil
evalToBool :: Val -> Bool
evalToBool (Num n) = n > 0
evalToBool Nil = False
evalToBool (Cons _ _) = True

-- Handled by Eq class
-- === operator is nonexistent, so must be strict
evalEqual :: Val -> Val -> Bool
evalEqual v1 v2 = (v1 == v2)

-- Only a Num is a true number, duh
evalIsNum :: Val -> Bool
evalIsNum (Num _) = True
evalIsNum _ = False

-- Comparison ------------------------------------------------------------------

-- Num are compared using their Int value.
-- Cons are compared with corresponding cells sequentially from left to right.
-- Every valid comparison implicitly follows this rule:
--    if a is not <> b then a == b
-- If a is not greater or less than b, it must strictly be equal.
-- This is not the case with the supplied abcdi, but due to the flexibility in
-- how we approached comparing two different Val, I have chosen this method.
-- Nil is less than all other Val, since something (even Num 0) > nothing.
-- This allows a variable to be set to Nil initially, then to 0 and compared
-- with the initial case.
-- Cons and Num cannot be validly compared - the process is ambiguous.
-- Note that these functions return MaybeIOK Bool not MaybeIOK Val - they are
-- transformed into Num using the Boolean functions above in evalExpr.

evalLess :: Val -> Val -> MaybeIOK Bool
evalLess (Num a) (Num b) = IOK (a < b)
evalLess (Cons a b) (Cons c d) = do
	l <- evalLess a c
	r <- evalLess b d
	IOK (l || r)
evalLess Nil Nil = IOK False
evalLess Nil _ = IOK True
evalLess _ Nil = IOK False
evalLess a b = IError $
	"invalid comparison: (" ++ (strVal a) ++ ") < (" ++ (strVal b) ++ ")"

evalGreater :: Val -> Val -> MaybeIOK Bool
evalGreater (Num a) (Num b) = IOK (a > b)
evalGreater (Cons a b) (Cons c d) = do
	l <- evalGreater a c
	r <- evalGreater b d
	IOK (l || r)
evalGreater Nil Nil = IOK False
evalGreater Nil _ = IOK False
evalGreater _ Nil = IOK True
evalGreater a b = IError $
	"invalid comparison: (" ++ (strVal a) ++ ") > (" ++ (strVal b) ++ ")"

-- Cons ------------------------------------------------------------------------

-- Heads and tails are only considered for Cons, a Num or Nil has neither.

evalHead :: Val -> MaybeIOK Val
evalHead (Cons a _) = IOK a
evalHead x = IError $ "no head: " ++ (strVal x) ++ ")"

evalTail :: Val -> MaybeIOK Val
evalTail (Cons _ b) = IOK b
evalTail x = IError $ "no tail: tail(" ++ (strVal x) ++ ")"

-- Statements ------------------------------------------------------------------

-- Each statement is evaluated, returning the new Context, with updated Vars,
-- which is passed to the next statement until there are none left.
-- Any IError from an Expr propogates upwards.

evalStmts :: [Stmt] -> Context -> MaybeIOK Context
evalStmts [] c = IOK c
evalStmts [st] c@(Context p vs fn d sk) =
	case st of
		Assign v e -> do
			nv <- evalExpr e c
			printDebug c ((strVar v) ++ " := " ++ (strVal nv))
			-- A new Context created with updated Var
			IOK (Context p (setVar v nv vs) fn d sk)
		ITE e s1 s2 -> do
			nv <- evalExpr e c
			if evalToBool nv then
				evalStmts s1 c
			else
				evalStmts s2 c
		While e ss -> do
			nv <- evalExpr e c
			-- The while condition
			if evalToBool nv then do
				-- Eventual context for statements at end of while
				nc <- evalStmts ss c
				-- Another while is generated with updated Context
				evalStmts [While e ss] nc
			else IOK c
evalStmts (st:ss) c = do
	-- A list of stmts are handled sequentially, passing the new Context
	nc <- evalStmts [st] c
	evalStmts ss nc
		
-- Functions -------------------------------------------------------------------

-- Runs a function and returns the final Context, or an IError. Only the A Var
-- is returned (see below).
evalFunc :: Func -> Context -> MaybeIOK Context
evalFunc f c = evalStmts (func_body f) c

-- Finds a function by name in given Prog, or returns Nothing.
getFunc :: Prog -> String -> Maybe Func
getFunc (Prog fs) s = getFunc' fs s

getFunc' :: [Func] -> String -> Maybe Func
getFunc' [] _ = Nothing
getFunc' (f:fs) s = 
	if func_name f == s then
		Just f
	else
		getFunc' fs s

-- Higher order, finds and calls a function if found, else return an IError
runFunc :: String -> Context -> MaybeIOK Context
runFunc newFn c@(Context p vs fn d sk) = 
	let maybeFunc = getFunc p newFn in 
		case maybeFunc of
			-- improve
			Nothing -> IError $ "no " ++ fn ++ " function"
			Just func -> do
				when (sk > 0) $ printDebug c ("call " ++ newFn)
				-- Increments the call stack count, evaluates function
				newContext <- (evalFunc func (Context p vs fn d (sk+1)))
				when (sk > 0) $ printDebug c ("return-from " ++ newFn)
				-- Returns only Var A in new Context, all else are Nil
				IOK $ Context p (Vars (getVar A (cVars newContext)) Nil Nil Nil)
					fn d sk

-- VARIABLES ===================================================================

-- Returns Val for specified Var from Vars
getVar :: Var -> Vars -> Val
getVar v (Vars a b c d) =
	case v of
		A -> a
		B -> b
		C -> c
		D -> d

-- Retains given Vars, updates Val only for specified Var
setVar :: Var -> Val -> Vars -> Vars
setVar var val (Vars a b c d) = 
	case var of
		A -> Vars val b c d
		B -> Vars a val c d
		C -> Vars a b val d
		D -> Vars a b c val

-- PRINTING ====================================================================

-- Stringifies a Val into ABCD syntax
strVal :: Val -> String
strVal (Num x) = show x
strVal Nil = "[]"
-- Cons are left associative, surrounded in parenthesis
strVal (Cons a b) = "(" ++ (strVal a) ++ "^" ++ (strVal b) ++ ")"

-- Lowercase representation of variables
strVar :: Var -> String
strVar v = map toLower $ show v

strVars :: Vars -> String
strVars (Vars a b c d) =
	"A: " ++ strVal a ++ " B: " ++ strVal b ++ " C: " ++ strVal c ++ " D: "
		++ strVal d

-- Returns a string self-concatenated n times
multiStr :: String -> Int -> String
multiStr str n
	| n > 0 = str ++ (multiStr str (n-1))
	| otherwise = ""

-- DEBUGGING -------------------------------------------------------------------

-- Used in the evaluation functions and is designed to:
-- 1) Print a given string, using the call stack count to indent
-- 1) Cause no side-effects
-- 2) Keep IO independent of the internal evaluation functions
printDebug :: Context -> String -> MaybeIOK ()
printDebug (Context _ _ _ debug stack) str = unsafePerformIO $ do
	case debug of
		-- 0 is init, 1 is root function
		Debug -> putStrLn $ (multiStr " " (stack-1)) ++ str
		_ -> return ()
	return $ IOK ()

-- TESTS =======================================================================

-- Allows testing evaluation without external files.

-- A wrapper type for Expr, Stmt, Func - and a given Context.
data TestingType = 
	TestExpr Expr Context |
	TestStmt Stmt Context | 
	TestFunc Func Context

-- List of tests to execute
interpretTests :: [TestingType]
interpretTests =
	[
		TestExpr ( NumE 23 ) testContext,
		TestExpr ( Minus (NumE 23) (NumE 24) ) testContext,
		TestExpr ( Plus (NumE 23) (NumE 24) ) testContext,
		TestExpr ( Plus (NumE 23) (ConsE NilE NilE) ) testContext,
		TestExpr ( Equal (NumE 2) (NumE 2) ) testContext,
		TestExpr ( Equal (NumE 2) (NumE 3) ) testContext,
		TestExpr ( Times (NumE 23) (NumE 24) ) testContext,
		TestExpr ( Div (NumE 30) (NumE 4) ) testContext,
		TestExpr ( Less (NumE 2) (NumE 3) ) testContext,
		TestExpr ( ConsE (NumE 23) (NumE 24) ) testContext,
		TestExpr ( Less (ConsE (NumE 2) (ConsE (NumE 2) (NilE)))
			(ConsE (NumE 2) (ConsE (NumE 3) (NilE))) ) testContext,
		TestExpr ( Greater (ConsE (NumE 2) (ConsE (NumE 3) (NilE)))
			(ConsE (NumE 2) (ConsE (NumE 2) (NilE))) ) testContext,
		TestExpr ( Not (NumE (-23)) ) testContext,
		TestExpr ( Isnum (NumE (-23)) ) testContext,
		TestExpr ( And (NumE 1) (NumE 0) ) testContext,
		TestExpr ( Or (NumE 1) (NumE 0) ) testContext,
		TestExpr ( Head (ConsE (NumE 1) (NumE 2)) ) testContext,
		TestExpr ( Tail (ConsE (NumE 2) (ConsE (NumE 2) (NilE))) ) testContext,
		TestStmt ( Assign A (NumE 23) ) testContext,
		TestStmt ( ITE (Var A) [Assign A (NumE 20)] [Assign B (NumE 23)] )
			testContext,
		TestStmt ( While (Less (Var A) (NumE 2)) [
			Assign A (Plus (Var A) (NumE 1)),
			Assign B (Plus (Var A) (NumE 1))] ) testContext,
		TestFunc ( Func "testFunc" [Assign A (NumE 23)] ) testContext
	]

-- Run this function to print each test in interpretTests
interpretTest :: IO ()
interpretTest = do
	putStrLn "\n *** TESTS! ***"
	interpretTest' interpretTests

-- Prints output for each test case and result
interpretTest' :: [TestingType] -> IO ()
interpretTest' [] = return ()
interpretTest' (x:xs) = do
	putStrLn (">>> " ++ (strTestingType x))
	case x of
		TestExpr e c ->
			case (evalExpr e c) of
				IError s -> putStrLn s
				IOK v -> putStrLn $ "\t" ++ strVal v
		TestStmt st c -> do
			let mc = evalStmts [st] c
			case mc of
				IError s -> putStrLn s
				IOK nc -> putStrLn $ "\t" ++ strVars (cVars nc)
		TestFunc f c -> do
			let mc = evalFunc f c
			case mc of
				IError s -> putStrLn s
				IOK nc -> putStrLn $ "\t" ++ strVal (getVar A (cVars nc))
	interpretTest' xs

-- Returns the string representation of underlying TestingType
strTestingType :: TestingType -> String
strTestingType t =
	-- show $ ttest t
	case t of
		TestExpr e _ -> show e
		TestStmt s _ -> show s
		TestFunc f _ -> show f

-- MISC ========================================================================

-- Ternary operator
(?) :: Bool -> (t, t) -> t
a ? (b, c) = if a then b else c

-- Default testing Vars
nilVars :: Vars
nilVars = Vars Nil Nil Nil Nil

-- Default testing Context
testContext :: Context
testContext = Context (Prog []) nilVars "Test" Debug 0

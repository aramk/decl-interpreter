<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<title>COMP30020 Declarative Programming Project 1</title>
<link rev=made href="mailto:lee">
</head>
<body>

<center>
<h1>COMP30020 Declarative Programming</h1>
<h1>Project 1</h1>
</center>

<h2>Aim</h2>

The primary aim of this project is for you to get experience with
understanding and writing Haskell code.  An additional aim is for you
to gain a deeper understanding of different styles of programming in
different programming language paradigms and understand how we can
precisely specify the meaning of imperative code.
<p>

<h2>Outline</h2>

Your task is to implement an interpreter for a simple programming
language, called ABCD.  ABCD is an imperative-style language bearing some
resemblance to C and Pascal.  It has assignment statements, while loops,
conditionals and sequencing (a statement followed by another statement).
It supports numbers, lists, and general binary tree structures
and has a few basic operations on these values built in.
It does not have a static type system,
so the same variable can hold values of different types at different times.
An unusual aspect of ABCD is that it has only four variables,
<code>a</code>, <code>b</code>, <code>c</code> and <code>d</code>.
Perhaps surprisingly, this combination of four variables,
minimal set of statement types and the supported basic operations
is in theory as powerful as any other programming language;
it is "Turing complete", which means that it can express any computation.
However, it is rather restrictive in the programming styles it supports.
Data structures can only be constructed in a "bottom up" way.
For example, to construct a list, you must completely construct
the tail of the list before adding the head element.
<p>

Additionally, the version used for this project is extended with
functions and a simple form of (recursive) function call (perhaps it
should be called ABCDEF).  This makes it possible to code many problems
more naturally, constructing data structures "top down", for example.
<p>

<h2>ABCD Examples</h2>

Below are three ABCD programs (more will be provided online in
<code>/home/subjects/comp30020/projects/abcd_progs</code> on
<code>datura.eng.unimelb.edu.au</code>).
The first takes two integers <code>a</code> and <code>b</code>
and computes the greatest common divisor, leaving the result in both variables:
<pre>
func gcd
begin
    while not a = b
    do
        if b < a then
            a := a - b;
        else
            b := b - a;
        endif
    done
end
</pre>
<p>

The next takes a list <code>a</code> and reverses it using <code>b</code>
as a temporary variable.  The empty list is written <code>[]</code>
and the list construction operation is written <code>^</code> (similar
to <code>:</code> in Haskell), so <code>1^(2^(3^[])))</code> is the list
containing the numbers 1, 2 and 3.
<pre>
func reverse
begin
    b := [];            -- [] is the empty list (and empty tree)
    while not a = []
    do
        b := head(a)^b; -- ^ is the list (+tree) constructor
        a := tail(a);   -- head/tail like in Haskell also
    done
    a := b;             -- a is now the reverse of the original list
end
</pre>
<p>

ABCD functions have no explicit arguments declared
and they do not explicitly return any value.
They are implicitly called with the four variables
and implicitly return variable <code>a</code> when the call terminates.
Variables are local to a particular function invocation,
like "auto" variables in C:
assignments only modify the local copies
and do not affect the values of the variables in the calling context.
The next program uses recursion to append the two lists in variables
<code>b</code> and <code>c</code>.
<pre>
func append
-- a,b,c,d implicitly passed in, though
-- only the values of b and c are used
begin
    if not b == [] then
        d := head(b);   -- this uses d as a temporary variable
        b := tail(b);
        a := d^call(append); -- call(append) is like calling append(a,b,c,d)
    else
        a := c;
    endif
-- implicit "return a"
end
</pre>
<p>

The syntax of ABCD programs is formally defined below.
(* means zero or more times,
+ means one or more times,
? means zero or one times).
ABCD also has Haskell-style comments,
which start from "--" and extend until the end of the line.
<p>

<pre>
prog ::= func+
func ::= "func" id "begin" stmt* "end"
stmt ::= var ":=" expr ";" |
	"if" expr "then" stmt* (else stmt*)? "endif" |
	 "while" expr "do" stmt* "done"
expr ::= var | num | "[]" |
	"head" "(" expr ")" | "tail" "(" expr ")" |
        "isnum" "(" expr ")" | "call" "(" id ")" | "(" expr ")" |
	expr "*" expr | expr "/" expr |
	expr "+" expr | expr "-" expr |
	expr "<" expr | expr "=" expr | expr ">" expr |
	"not" expr |
	expr "and" expr |
	expr "or" expr |
	expr "^" expr
var ::= "a" | "b" | "c" | "d"
num ::= sequence of one or more digits
id ::= letter/underscore followed by letters/underscores/digits
</pre>
<p>

The primitive expressions are variables, number constants, the empty list
constant and calls; the calls may be to the builtin functions "head",
"tail", "isnum" (which tests if its argument is a number) or to a function
defined in the ABCD program.  You can build more complex expressions
from these by using parentheses and a variety of operators, some unary,
most binary.  The operators listed on each line in the definition above
have the same priority, with operators on earlier lines having higher
priority, which means that e.g. <code>not a or b</code> means <code>(not
a) or b</code>.  All the binary operators are left associative, which
means that e.g. <code>a - b - c</code> means <code>(a - b) - c</code>,
not <code>a - (b - c)</code>.
<p>

Left associativity means that paretheses must be used when writing lists,
for example <code>1^2^3^[]</code> is the same as <code>((1^2)^3)^[]</code>
which is not a list, though it is a valid value.  The values that
ABCD programs operate on are called "lumbers", since they are often
lists or numbers but can be more general tree structures.  In general,
a lumber is a number, the empty list, or a "cons cell" <code>l1^l2</code>
where both <code>l1</code> and <code>l2</code> are lumbers.  Note that
<code>^</code> can be used as a general pairing operator (unlike Haskell,
the right operand doesn't have to be a list) allowing general binary
trees such as <code>((1^2)^3)^[]</code>.  The head and tail operations
can be used to extract left and right subtrees, respectively.
<p>

<h2>ABCD semantics</h2>

The meaning of an ABCD statement (including compound statements)
can be given by the mapping
from the initial values of the variables
to the final value of the variables.
The meaning of an ABCD function is a mapping from
the initial values of the variables to the final value of <code>a</code>.
The meaning of an ABCD expression
is a mapping from the values of the variables to a single value.
Data structures cannot be "destructively" updated;
only variables can be assigned new values.
Also, data structures cannot have any parts which are uninitialised,
since there is no explicit <code>malloc</code> like C.
These restrictions make the semantics significantly simpler,
but also restrict programming style.
<p>

The interpreter you will code can be considered
a precise logical definition of the semantics,
and can also be executed using Haskell.
The code you will write will have a function which takes an ABCD expression,
say <code>a-b</code>, and the current values of the variables,
say &lt;7,2,0,0&gt;
and returns the value of the expression, which is 5 in this case.
(This task is similar to one of the workshop questions.)
Similarly, you will write a function
which takes an ABCD statement and the current values of the variables
and returns the value of the variables after the statement is executed.
<p>

Your interpreter should support all the arithmetic and comparison
operations on numbers, and the usual boolean operations ("and", "or"
and "not").  These boolean operations and comparison operations should
represent their result as "1" for true and "0" for false.  We leave
the behaviour of some ABCD programs unspecified.  For example, how
values other than 0 and 1 are dealt with in contexts where booleans
are expected.
<p>

Some operations are clearly wrong,
such as applying the builtin function "head" to something other than a
cons cell.
In such cases, the interpreter should print an error message and abort.
Others are questionable.
For example, what should the interpreter do
when the operands of "+" are both lists?
There is more than one reasonable answer.
It could for example append the lists,
or it could print an error message and abort.
You may make your own decisions on what to do in such situations,
as long as
<ul>
<li>
your interpreter can execute the ABCD code supplied to you, and
<li>
you clearly document whatever decisions you make
concerning these more subtle parts of the semantics.
</ul>
<p>

Your interpreter will be a stand-alone program
which takes a filename as the command line argument.
The interpreter will expect the named file to contain an ABCD program,
and it will read in the program.
The interpreter will then
read in the initial values of the four variables from standard input
(using the same syntax used in ABCD programs),
simulate a call to the selected function in the program
(which will be the function <code>main</code>
unless an optional command-line argument of the form "-e funcname"
specifies a different function as the entry point to the program),
and print the value returned from the entry-point function to standard output.
In addition, the program should accept
an optional <code>-d</code> command line argument.
If this option is given, the program should be interpreted in "debug" mode,
which means that
<ul>
<li>
after every assignment statement is executed,
the interpreter should print a line specifying
the variable being assigned to and its new value,
<li>
when making a call to a user-defined function,
the interpreter should print a line specifying
the function being called, and
<li>
when returning from a call,
the interpreter should print a line specifying
the function which is returning.
</ul>
A sample will be supplied - please try to reproduce similar output.
A "-p" option, which prints the internal representation of the program
before proceeding as above should also be supported.
<p>

<h2>Supplied code</h2>

We will supply you with a skeleton for your program,
and some ABCD sample programs in
<code>/home/subjects/comp30020/projects/proj1</code>.
The largest component is a parser (<code>Parse.hs</code>)
for ABCD programs
which converts the source syntax to an internal representation
(which is defined in <code>Prog.hs</code>).
Command line processing and the top level code is in <code>Main.hs</code>.
You are strongly encouraged to look at this code;
you cannot even start writing your interpreter
without understanding the internal representation,
and reading the other modules will be educational as well.
You <strong>should not modify</strong>
any of these files and should not submit them for assessment.
<p>

<h2>The task</h2>

The fourth module of the program,
<code>Interpret.hs</code>,
is intended to contain an interpreter for ABCD programs as described above.
The supplied form of this module is only a skeleton.
Your task is to complete and submit this module,
although if you prefer to use literate Haskell,
you can instead submit a literate programming version of this file
as <code>Interpret.lhs</code>
The supplied code plus your modified file should work under
<code>ghc</code> on the <code>datura.eng.unimelb.edu.au</code> server.
You should test it on that server before submitting; it may behave
slightly differently to the ghc versions installed elsewhere.
<p>

<h2>Marking scheme</h2>

This project is worth 10 marks (10% of the overall assessment for the subject).
You can obtain up to 6 marks for an ABCD interpreter
which does not support function calls or the debug option.
It must still be able to read in a program
and execute the entry-point function correctly
for programs in which the entry-point function
has no instances of <code>call</code>,
and should exhibit good programming style.
Up to two additional marks are available if function calls are supported
and up to two additional marks are available if debugging is supported.
<p>

<h2>Questions and answers</h2>

Questions and answers pertaining to the project will be available on
<a href="./QandA.html">the web page</a>
and will be considered part of the specification of the project.
The LMS discussion forum can also be a very valuable resource.
Questions about the project are best asked in a workshop
or the discussion forum.
<p>

<h2>The submitted file</h2>

Your program <strong>must</strong> be in a file called
<code>Interpret.hs</code> or <code>Interpret.lhs</code>.  This file should
include a comment near the top that gives your name.  The line giving
your name, as well as any other line that identifies you and/or that you
wish to hide from others should be between a line that contains the string
HIDE_START and a line that contains the string HIDE_END.  After everyone
has submitted their projects, we intend to make all submissions available
to everyone enrolled in the subject <em>after</em> stripping out of them
all lines between HIDE_START and HIDE_END.  This should allow everyone
to compare their submissions with everyone else's.
<p>

<h2>Submission</h2>

Submit just <code>Interpret.hs</code> or <code>Interpret.lhs</code>
using the command
<pre>
submit comp30020 1
</pre>
on datura.eng.unimelb.edu.au by
<em>11am on Thursday, April 26</em>.
Verify your submission using the command <code>verify comp30020 1</code>.
Note that <em>many</em> people take some time to get used to Haskell,
so doing the workshop exercises and getting an early start on this project
are strongly recommended.
<p>

Late submissions will incur a penalty
of two marks per day (or part thereof) late.
If you cannot submit on time,
you should contact the lecturer at the soonest possible opportunity;
in the absence of really extraordinary circumstances,
this means <em>before</em> the deadline.
Late submission can be done using the command <code>submit comp30020 1.late</code>.
<p>

This is an individual project.  You may discuss algorithms, the
supplied code, overall code structure and testing with colleagues,
but all submitted code must be your own.  You should be aware of
university policies concerning plagiarism.
<p>

</html>

INTERPFILE = Interpret.hs
# INTERPFILE = Interpret.lhs
INTERPRETER = Prog.hs Parse.hs $(INTERPFILE) Main.hs
GHC_FLAGS = -Wall -Werror \
	-fwarn-incomplete-patterns \
	-fwarn-name-shadowing \
	-fno-warn-unused-binds

abcdi:	$(INTERPRETER)
	ghc --make $(GHC_FLAGS) $(INTERPRETER) -o abcdi
